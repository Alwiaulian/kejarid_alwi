class StudentsController < ApplicationController
  
  def new #menampilkan form data baru
    @student = Student.new
  end
  def show #menampikan sebuah data secara detail
    id = params[:id]
    @student = Student.find id
    #render plain: id
  end

  #untuk memperoses data baru yg dimasukkan di form new
  def create 
    student = Student.new(resources_params)
    student.save
    flash[:notice]= 'Data sudah tersimpan'
    redirect_to students_path
  end

  def edit #menampilkan daya yg sudah disimpan di edit
    @student = Student.find(params[:id])
  end

  def update #melakukan proses ketika user mengedit data
    @student = Student.find(params[:id])
    @student.update(resources_params)

    flash[:notice]= 'Data sudah terupdate'
    redirect_to student_path(@student)

  end
  def destroy #untuk menghapus data
    @student = Student.find(params[:id])
    @student.destroy        

    flash[:notice]= 'Data sudah terhapus'

    redirect_to students_path
    
end

  def index
    @students = Student.all
  end

  private
  def resources_params
      params.require(:student).permit(:name, :username, :age, :kelas, :address, :city, :nik)
  end



  
end
