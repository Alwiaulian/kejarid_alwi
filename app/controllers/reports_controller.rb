class ReportsController < ApplicationController
  def new #menampilkan form data baru
    @report = Report.new
  end
  def show #menampikan sebuah data secara detail
    id = params[:id]
    @report = Report.find id
    #render plain: id
  end

  #untuk memperoses data baru yg dimasukkan di form new
  def create 
    report = Report.new(resources_params)
    report.save
    flash[:notice]= 'Data sudah tersimpan'
    redirect_to reports_path
  end

  def edit #menampilkan daya yg sudah disimpan di edit
    @report = Report.find(params[:id])
  end

  def update #melakukan proses ketika user mengedit data
    @report = Report.find(params[:id])
    @report.update(resources_params)

    flash[:notice]= 'Data sudah terupdate'
    redirect_to report_path(@report)

  end
  def destroy #untuk menghapus data
    @report = Report.find(params[:id])
    @report.destroy        

    flash[:notice]= 'Data sudah terhapus'

    redirect_to reports_path
    
end

  def index
    @reports = Report.all
  end

  private
  def resources_params
      params.require(:report).permit(:title, :hasil, :mapel, :teacher_id, :student_id, :tanggal)
  end


end
