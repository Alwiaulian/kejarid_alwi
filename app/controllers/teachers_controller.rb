class TeachersController < ApplicationController
  def new #menampilkan form data baru
    @teacher = Teacher.new
  end
  def show #menampikan sebuah data secara detail
    id = params[:id]
    @teacher = Teacher.find id
    #render plain: id
  end

  #untuk memperoses data baru yg dimasukkan di form new
  def create 
    teacher = Teacher.new(resources_params)
    teacher.save
    flash[:notice]= 'Data sudah tersimpan'
    redirect_to teachers_path
  end

  def edit #menampilkan daya yg sudah disimpan di edit
    @teacher = Teacher.find(params[:id])
  end

  def update #melakukan proses ketika user mengedit data
    @teacher = Teacher.find(params[:id])
    @teacher.update(resources_params)

    flash[:notice]= 'Data sudah terupdate'
    redirect_to teacher_path(@teacher)

  end
  def destroy #untuk menghapus data
    @teacher = Teacher.find(params[:id])
    @teacher.destroy        

    flash[:notice]= 'Data sudah terhapus'

    redirect_to teachers_path
    
end

  def index
    @teachers = Teacher.all
  end

  private
  def resources_params
      params.require(:teacher).permit(:nik, :name, :age, :kelas, :mapel)
  end

end
