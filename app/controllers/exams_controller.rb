class ExamsController < ApplicationController
  def new #menampilkan form data baru
    @exam = Exam.new
  end
  def show #menampikan sebuah data secara detail
    id = params[:id]
    @exam = Exam.find id
    #render plain: id
  end

  #untuk memperoses data baru yg dimasukkan di form new
  def create 
    exam = Exam.new(resources_params)
    exam.save
    flash[:notice]= 'Data sudah tersimpan'
    redirect_to exams_path
  end

  def edit #menampilkan daya yg sudah disimpan di edit
    @exam = Exam.find(params[:id])
  end

  def update #melakukan proses ketika user mengedit data
    @exam = Exam.find(params[:id])
    @exam.update(resources_params)

    flash[:notice]= 'Data sudah terupdate'
    redirect_to exam_path(@exam)

  end
  def destroy #untuk menghapus data
    @exam = Exam.find(params[:id])
    @exam.destroy        

    flash[:notice]= 'Data sudah terhapus'

    redirect_to exams_path
    
end

  def index
    @exams = Exam.all
  end

  private
  def resources_params
      params.require(:exam).permit(:title,:mapel, :duration, :nilai, :status, :level, :student_id )
  end



 
end
