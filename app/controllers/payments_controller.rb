class PaymentsController < ApplicationController
  def new #menampilkan form data baru
    @payment = Payment.new
  end
  def show #menampikan sebuah data secara detail
    id = params[:id]
    @payment = Payment.find id
    #render plain: id
  end

  #untuk memperoses data baru yg dimasukkan di form new
  def create 
    payment = Payment.new(resources_params)
    payment.save
    flash[:notice]= 'Data sudah tersimpan'
    redirect_to payments_path
  end

  def edit #menampilkan daya yg sudah disimpan di edit
    @payment = Payment.find(params[:id])
  end

  def update #melakukan proses ketika user mengedit data
    @payment = Payment.find(params[:id])
    @payment.update(resources_params)

    flash[:notice]= 'Data sudah terupdate'
    redirect_to payment_path(@payment)

  end
  def destroy #untuk menghapus data
    @payment = Payment.find(params[:id])
    @payment.destroy        

    flash[:notice]= 'Data sudah terhapus'

    redirect_to payments_path
    
end

  def index
    @payments = Payment.all
  end

  private
  def resources_params
      params.require(:payment).permit(:id_transaction, :status, :upload)
  end
end
