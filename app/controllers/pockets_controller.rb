class PocketsController < ApplicationController
  def new #menampilkan form data baru
    @pocket = Pocket.new
  end
  def show #menampikan sebuah data secara detail
    id = params[:id]
    @pocket = Pocket.find id
    #render plain: id
  end

  #untuk memperoses data baru yg dimasukkan di form new
  def create 
    pocket = Pocket.new(resources_params)
    pocket.save
    flash[:notice]= 'Data sudah tersimpan'
    redirect_to pockets_path
  end

  def edit #menampilkan daya yg sudah disimpan di edit
    @pocket = Pocket.find(params[:id])
  end

  def update #melakukan proses ketika user mengedit data
    @pocket = Pocket.find(params[:id])
    @pocket.update(resources_params)

    flash[:notice]= 'Data sudah terupdate'
    redirect_to pocket_path(@pocket)

  end
  def destroy #untuk menghapus data
    @pocket = Pocket.find(params[:id])
    @pocket.destroy        

    flash[:notice]= 'Data sudah terhapus'

    redirect_to pockets_path
    
end

  def index
    @pockets = Pocket.all
  end

  private
  def resources_params
      params.require(:pocket).permit(:balance, :student_id, :teacher_id)
  end
end
