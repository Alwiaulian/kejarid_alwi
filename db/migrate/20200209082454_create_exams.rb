class CreateExams < ActiveRecord::Migration[6.0]
  def change
    create_table :exams do |t|
      t.string :title
      
      t.string :mapel
      
      t.string :duration
      
      t.string :nilai
      
      t.string :status
      
      t.string :level
      
      t.string :student_id
      

      t.timestamps
    end
  end
end
