class CreateStudents < ActiveRecord::Migration[6.0]
  def change
    create_table :students do |t|
      t.string :name
      t.string :username
      t.string :age
      t.string :kelas
      t.string :address
      t.string :city
      t.string :nik
      
      t.timestamps
    end
  end
end
