Rails.application.routes.draw do
  get 'pockets/index'
  get 'pockets/new'
  get 'pockets/show'
  get 'pockets/edit'
  get 'payments/index'
  get 'payments/new'
  get 'payments/show'
  get 'payments/edit'
  resources :exams
  resources :students
  resources :teachers
  resources :reports
  resources :payments
  resources :pockets
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
